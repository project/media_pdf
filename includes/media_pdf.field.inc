<?php
/**
 * Implements hook_field_formatter_info().
 */
function media_pdf_field_formatter_info() {
  return array(
    'media_pdf_default' => array(
      'label' => t('PDF default'),
      'field types' => array('file'),
    ),
    'media_pdf_thumbnail' => array(
      'label' => t('PDF first page'),
      'field types' => array('file'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function media_pdf_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'media_pdf_default':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'media_pdf_formatter_default',
          '#file' => $item,
        );
      }
      break;
    case 'media_pdf_thumbnail':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'media_pdf_formatter_thumbnail',
          '#file' => $item,
        );
      }
      break;
  }
  return $element;
}

function theme_media_pdf_formatter_default($variables) {
}

function theme_media_pdf_formatter_thumbnail($variables) {
  drupal_add_js(array('media_pdf' => array('pdf_js' => 'http://localhost/~lshen/ebi7/sites/all/libraries/pdf.js/pdf.js')), 'setting');

  drupal_add_js('sites/all/libraries/pdf.js/pdf.js');
  global $base_url;
  $file = file_create_url($variables['file']['uri']);
  $worker_loader = $base_url . '/' . drupal_get_path('module', 'media_pdf') . '/js/worker_loader.js';
      
//  drupal_add_js("", array('type' => 'inline'));
//  drupal_add_js("PDFJS.workerSrc = '$worker_loader'", array('type' => 'inline'));
//  drupal_add_js('sites/all/libraries/pdf.js/worker_loader.js');
    //dpm($file);
//  $js = "importScripts('http://localhost/~lshen/ebi7/sites/all/libraries/pdf.js/pdf.js');";

  $js = "PDFJS.workerSrc = '$worker_loader';";
  $js .= "'use strict';
    PDFJS.getPdf('$file', function getPdfHelloWorld(data) {
    var pdf = new PDFJS.PDFDoc(data);
    var page = pdf.getPage(1);
    var scale = 1.5;
    var canvas = document.getElementById('media-pdf');
    var context = canvas.getContext('2d');
    canvas.height = page.height * scale;
    canvas.width = page.width * scale;
    page.startRendering(context);
  });
  ";
  drupal_add_js($js, array('type' => 'inline'));
  $output = '<canvas id="media-pdf" style="border:1px solid black;"/>';
//  $output = '<canvas id="the-canvas" style="border:1px solid black;"/>';
  return $output;
}

